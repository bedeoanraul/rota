import Vue from 'vue'
import Vuex from 'vuex'

import members from './store-members'
import settings from './store-settings'
import auth from './store-auth'
import cofees from './store-cofees'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      members,
      settings,
      auth,
      cofees
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  })

  return Store
}
