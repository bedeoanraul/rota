import Vue from 'vue'
import { uid, Notify } from 'quasar'
import { firebaseDb, firebaseAuth } from 'boot/firebase'
import { showErrorMessage } from 'src/functions/function-show-error-message'

const state = {
	cofees: {},
	search: '',
	sort: 'name',
	cofeesDownloaded: false
}

const mutations = {
	updateCofee(state, payload) {
		Object.assign(state.cofees[payload.id], payload.updates)
	},
	deleteCofee(state, id) {
		Vue.delete(state.cofees, id)
	},
	addCofee(state, payload) {
		Vue.set(state.cofees, payload.id, payload.Cofee)
	},
	clearcofees(state) {
		state.cofees = {}
	},
	setSearch(state, value) {
		state.search = value
	},
	setSort(state, value) {
		state.sort = value
	},
	setcofeesDownloaded(state, value) {
		state.cofeesDownloaded = value
	}
}

const actions = {
	updateCofee({ dispatch }, payload) {
		dispatch('fbUpdateCofee', payload)
	},
	deleteCofee({ dispatch }, id) {
		dispatch('fbDeleteCofee', id)
  },
  deleteAllCofees({ dispatch }, id) {
    Object.keys(state.cofees).forEach(id => {
      dispatch('fbDeleteCofee', id)
    });
	},
	addCofee({ dispatch }, Cofee) {
		let CofeeId = uid()
		let payload = {
			id: CofeeId,
			Cofee: Cofee
		}
		dispatch('fbAddCofee', payload)
	},
	setSearch({ commit }, value) {
		commit('setSearch', value)
	},
	setSort({ commit }, value) {
		commit('setSort', value)
	},

	fbReadDataCofees({ commit }) {
    console.log('da coffes')
		// let userId = firebaseAuth.currentUser.uid
		let cofees = firebaseDb.ref('cofees')
		// initial check for data
		cofees.once('value', snapshop => {
			commit('setcofeesDownloaded', true)
		}, error => {
			showErrorMessage(error.message)
			// this.$router.replace('/auth')
		})

		// child added
		cofees.on('child_added', snapshot => {
      let Cofee = snapshot.val()
			let payload = {
				id: snapshot.key,
				Cofee: Cofee
      }
			commit('addCofee', payload)
		})

		// child changed
		cofees.on('child_changed', snapshot => {
			let Cofee = snapshot.val()
			let payload = {
				id: snapshot.key,
				updates: Cofee
			}
			commit('updateCofee', payload)
		})

		// child removed
		cofees.on('child_removed', snapshot => {
			let CofeeId = snapshot.key
			commit('deleteCofee', CofeeId)
		})
	},
	fbAddCofee({}, payload) {
		// let userId = firebaseAuth.currentUser.uid
		let CofeeRef = firebaseDb.ref('cofees/' + payload.id)
		CofeeRef.set(payload.Cofee, error => {
			if (error) {
				showErrorMessage(error.message)
			}
			else {
				Notify.create('Cofee added!')
			}
		})
	},
	fbUpdateCofee({}, payload) {
		let CofeeRef = firebaseDb.ref('cofees/' + payload.id)
		CofeeRef.update(payload.updates, error => {
			if (error) {
				showErrorMessage(error.message)
			}
			else {
				let keys = Object.keys(payload.updates)
				if (!(keys.includes('completed') && keys.length == 1)) {
					Notify.create('Cofee updated!')
				}
			}
		})
	},
	fbDeleteCofee({}, CofeeId) {
		let CofeeRef = firebaseDb.ref('cofees/' + CofeeId)
		CofeeRef.remove(error => {
			if (error) {
				showErrorMessage(error.message)
			}
			else {
				Notify.create('Cofee deleted!')
			}
		})
  }
}

const getters = {
	cofees: (state, getters) => {
		return state.cofees
	}
}

export default {
	namespaced: true,
	state,
	mutations,
	actions,
	getters
}
