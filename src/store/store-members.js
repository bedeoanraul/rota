import Vue from 'vue'
import { uid, Notify } from 'quasar'
import { firebaseDb, firebaseAuth } from 'boot/firebase'
import { showErrorMessage } from 'src/functions/function-show-error-message'
import keyBy from 'lodash/keyBy'
const state = {
	members: {},
	search: '',
	sort: 'name',
	membersDownloaded: false
}

const mutations = {
	updateMember(state, payload) {
		Object.assign(state.members[payload.id], payload.updates)
	},
	deleteMember(state, id) {
		Vue.delete(state.members, id)
	},
	addMember(state, payload) {
		Vue.set(state.members, payload.id, payload.Member)
	},
	clearMembers(state) {
		state.members = {}
	},
	setSearch(state, value) {
		state.search = value
	},
	setSort(state, value) {
		state.sort = value
	},
	setMembersDownloaded(state, value) {
		state.membersDownloaded = value
	}
}

const actions = {
	updateMember({ dispatch }, payload) {
		dispatch('fbUpdateMember', payload)
	},
	deleteMember({ dispatch }, id) {
		dispatch('fbDeleteMember', id)
	},
	addMember({ dispatch }, Member) {
		let memberId = uid()
		let payload = {
			id: memberId,
			Member: Member
		}
		dispatch('fbAddMember', payload)
	},
	setSearch({ commit }, value) {
		commit('setSearch', value)
	},
	setSort({ commit }, value) {
		commit('setSort', value)
	},

	fbReadData({ commit }) {
    console.log('rota cofee')
		// let userId = firebaseAuth.currentUser.uid
		let members = firebaseDb.ref('members')
    console.log(members)
		// initial check for data
		members.once('value', snapshop => {
			commit('setMembersDownloaded', true)
		}, error => {
			showErrorMessage(error.message)
			// this.$router.replace('/auth')
		})

		// child added
		members.on('child_added', snapshot => {
      let Member = snapshot.val()
			let payload = {
				id: snapshot.key,
				Member: Member
      }
			commit('addMember', payload)
		})

		// child changed
		members.on('child_changed', snapshot => {
			let member = snapshot.val()
			let payload = {
				id: snapshot.key,
				updates: member
			}
			commit('updateMember', payload)
		})

		// child removed
		members.on('child_removed', snapshot => {
			let memberId = snapshot.key
			commit('deleteMember', memberId)
    })
	},
	fbAddMember({}, payload) {
		let MemberRef = firebaseDb.ref('members/' + payload.id)
		MemberRef.set(payload.Member, error => {
			if (error) {
				showErrorMessage(error.message)
			}
			else {
				Notify.create('Member added!')
			}
		})
	},
	fbUpdateMember({}, payload) {
		let userId = firebaseAuth.currentUser.uid
		let MemberRef = firebaseDb.ref('members/' + userId + '/' + payload.id)
		MemberRef.update(payload.updates, error => {
			if (error) {
				showErrorMessage(error.message)
			}
			else {
				let keys = Object.keys(payload.updates)
				if (!(keys.includes('completed') && keys.length == 1)) {
					Notify.create('Member updated!')
				}
			}
		})
	},
	fbDeleteMember({}, memberId) {
		let MemberRef = firebaseDb.ref('members/' + memberId)
		MemberRef.remove(error => {
			if (error) {
				showErrorMessage(error.message)
			}
			else {
				Notify.create('Member deleted!')
			}
		})
	}
}

const getters = {
	members: (state, getters) => {
		return state.members
  },
  membersWithId: (state, getters) => {
    let arrM = Object.entries(state.members);
      for (let [key, value] of arrM) {
        value.id = key;
      }
      return Object.fromEntries(arrM);
  }
}

export default {
	namespaced: true,
	state,
	mutations,
	actions,
	getters
}
