import * as firebase from "firebase/app"

import "firebase/auth"
import "firebase/database"

// ADD YOUR FIREBASE CONFIG OBJECT HERE:
var firebaseConfig = {
    apiKey: "AIzaSyBGzmKFMycLUGajOHpRnJt80FWH6DyN5AE",
    authDomain: "rotacofee.firebaseapp.com",
    databaseURL: "https://rotacofee.firebaseio.com",
    projectId: "rotacofee",
    storageBucket: "rotacofee.appspot.com",
    messagingSenderId: "1048180931484",
    appId: "1:1048180931484:web:2a1635fb3520f3e04ae659",
    measurementId: "G-G30F4Y7127"
}

let firebaseApp = firebase.initializeApp(firebaseConfig)
let firebaseAuth = firebaseApp.auth()
let firebaseDb = firebaseApp.database()

export { firebaseAuth, firebaseDb }
